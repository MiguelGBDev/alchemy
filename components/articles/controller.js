'use strict'

let mongoose = require('mongoose');
const articleModel = require('./model.js');

/*
let getOne = (req, res) => {

    let articleId = req.params.id;

    if (mongoose.Types.ObjectId.isValid(articleId))
        mongoose.Types.ObjectId(articleId);
    else
        return res.status(400).send({ message: 'Incorrect format ID' });

    articleModel.findById(articleId, (err, article) => {
        if(err)
            res.status(500).send({ message: 'Internal Error' });

        else {
            if (!article)
                res.status(404).send({message: 'Article not found'});
            else
                res.status(200).send(article);
        }
    });
}
*/

let getOne = (req, res) => {

    let articleId = req.params.id;

    if (mongoose.Types.ObjectId.isValid(articleId))
        mongoose.Types.ObjectId(articleId);
    else
        return res.status(400).send({ message: 'Incorrect format ID' });

    articleModel
        .findById(articleId, {_id: 0})      // 0 for hide userId
        .populate('author', {_id: 0})       // 0 for hide userId
        .exec( (err, article) => {
            if(err)
                res.status(500).send({ message: 'Internal Error' });

            else {
                if (!article)
                    res.status(404).send({message: 'Article not found'});
                else
                    res.status(200).send(article);
            }
    });
}

let get = (req, res) => {

    articleModel.find( {}, (err, articles) => {
        if(err)
            res.status(500).send({ message: 'Internal Error' });

        else {
            if (!articles)
                res.status(404).send({message: 'Empty articles'});

            res.status(200).send(articles);
        }
    })
}

let insert = (body, res) => {

    const newArticle = new articleModel(body);

    newArticle.save(function (err) {
        if(err)
            res.status(500).send({ message: 'Internal Error' });

        else
            res.status(200).send({newArticle});
    });
}

let modify = (req, res) => {

    let articleId = req.params.id;
    const mods = req.body;

    if (mongoose.Types.ObjectId.isValid(articleId))
        mongoose.Types.ObjectId(articleId);
    else
        return res.status(400).send({ message: 'Incorrect format ID' });

    articleModel.findByIdAndUpdate(articleId, mods, { new: true }, (err, article) => {
        if(err)
            res.status(500).send({ message: 'Internal Error' });

        else {
            if(!article)
                res.status(404).send({ message: 'Article not found' });

            else
                res.status(200).send({ article });
        }
    });
}

let remove = (req, res) => {

    let articleId = req.params.id;

    if (mongoose.Types.ObjectId.isValid(articleId))
        mongoose.Types.ObjectId(articleId);
    else
        return res.status(400).send({ message: 'Incorrect format ID' });

    articleModel.findById(articleId, (err, article) => {
        if(err)
            res.status(500).send({ message: 'Internal Error' });

        else{
            if (!article)
                res.status(404).send({ message: 'Article not found' });

            else{
                article.remove ( err2 => {
                    if(err2)
                        res.status(500).send({ message: 'Internal Error' });

                    else
                        res.status(200).send( { message: 'Article deleted' } );
                });
            }
        }
    });
}

module.exports = {
    get,
    getOne,
    insert,
    modify,
    remove
};
