let mongoose = require('mongoose');

let SourceSchema = mongoose.Schema({
    link: {type: String, required: true},
    name: {type: String, required: true}
},{ _id : false });

const ArticleSchema = mongoose.Schema({
    title: {type: String, required: true},
    author: { type : mongoose.Schema.Types.ObjectId, ref : 'user', required : true },
    body: {type: String, required: true},
    sources: [SourceSchema],
    tags: [ {type: String, enum: ['hardware', 'computers', 'phones', 'tablets', 'components', 'peripherals']} ]
}, { timestamps : true });

module.exports = mongoose.model('articles', ArticleSchema);