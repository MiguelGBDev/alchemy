'use strict'

let router = require('express').Router();
let Component = require('./');

router.route('/')
    .get((req, res) => {
        Component.get(req.query, res);
    })
    .post((req, res) => {
        Component.insert(req.body, res);
    });


router.route('/:id')
    .get((req, res) => {
        Component.getOne(req, res);
    })
    .patch((req, res) => {
        Component.modify(req, res);
    })
    .delete((req, res) => {
        Component.remove(req, res);
});

/*
router.route('/:id').checkId(req.params.id).patch((req, res) => {
    Component.modify(req, res);
});
*/

module.exports = router;
