'use strict'

let mongoose = require('mongoose');
const userModel = require('./model.js');

let getOne = (req, res) => {

    let userId = req.params.id;

    if (mongoose.Types.ObjectId.isValid(userId))
        mongoose.Types.ObjectId(userId);
    else
        return res.status(400).send({ message: 'Incorrect format ID' });

    userModel.findById(userId, (err, user) => {
        if(err) console.log(err);

        if (!user)
            res.status(404).send({ message: 'User not found' });
        else
            res.status(200).send( user );
    });
}

let get = (req, res) => {
    userModel.find( {}, (err, users) => {
        if(err) console.log(err);

        if (!users)
            res.status(404).send({ message: 'Empty users' });

        res.status(200).send( users );
    });
}

let insert = (body, res) => {

    const newUser = new userModel(body);

    newUser.save(function (err) {
        if(err) console.log(err);

        res.status(200).send({ newUser });
    });
}

let modify = (req, res) => {

    let userId = req.params.id;
    const mods = req.body;

    if (mongoose.Types.ObjectId.isValid(userId))
        mongoose.Types.ObjectId(userId);
    else
        return res.status(400).send({ message: 'Incorrect format ID' });

    userModel.findByIdAndUpdate(userId, mods, { new: true }, (err, user) => {
        if(err) console.log(err);

        if(!user)
            res.status(404).send({ message: 'User not found' });

        else
            res.status(200).send({ user });
    });
}

let remove = (req, res) => {    //Cron for one month instead

    let userId = req.params.id;

    if (mongoose.Types.ObjectId.isValid(userId))
        mongoose.Types.ObjectId(userId);
    else
        return res.status(400).send({ message: 'Incorrect format ID' });

    userModel.findById(userId, (err, user) => {
        if(err) console.log(err);

        if (!user)
            res.status(404).send({ message: 'User not found' });
        else{
            user.remove ( err2 => {
                if(err2) console.log(err2);

                res.status(200).send( { message: 'User deleted' } );
            });
        }
    });
}

module.exports = {
    getOne,
    get,
    insert,
    modify,
    remove
};
