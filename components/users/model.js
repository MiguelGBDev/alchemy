let mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    nombre: {type: String, required: true},
    apellido: {type: String, required: true}
}, { timestamps : true });

module.exports = mongoose.model('user', UserSchema);