module.exports = {

    project: {
        name: 'Alchemy API',
        url: 'localhost'
    },
    port: process.env.port || 3000,
    db: 'mongodb://localhost/alchemyDB',
    locale: {
        defaultLocale: 'es-ES'
    }
}
