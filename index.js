'use strict'

let conf = require('./conf');
process.env.ROOT = __dirname;

let mongoose = require('mongoose');
mongoose.connect(conf.db, {});

let api = require('./server')(conf);
api.start();

