'use strict'

let Api = {};
let express = require('express');

let bodyParser = require('body-parser');

function setApi(app, conf) {

    app.use(bodyParser.json());

    // parse apilication/x-www-form-urlencoded
    //app.use(bodyParser.urlencoded({extended: true, limit: config.upload.size}));

    // set the static files location /public will be / for users
    //app.use(express.static(__dirname + config.upload.path));

    app.use('/users', require('../components/users/routes'));
    app.use('/articles', require('../components/articles/routes'));
}

Api.start = function() {
    Api.express = express()
    const port = process.env.PORT || 3000

    setApi(Api.express, Api.conf);
    Api.express.listen(port);

    console.log(`Server is running for ${ Api.conf.project.name }:${ port }, Enjoy our API!`);

    /*
    console.log('Magic for %s happens on %s:%s/, fork: %s',
        Api.conf.project.name,
        Api.conf.apiAccess.url,
        port,
        process.env.pm_id || 'not forked');
        */
}

module.exports = function(conf){
    Api.conf = conf;
    return Api
}
